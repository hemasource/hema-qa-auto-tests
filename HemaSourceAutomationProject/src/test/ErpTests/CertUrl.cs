﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace HemaSourceAutomationProject.src.test.ErpTests
{
    public class CertUrlTest
    {
        private IWebDriver driver;
        public string packingSlipUrl = "https://hsi.hemasource.com/File/GetCert?type=SOC1323N%20Baxter%20Saline&name=Y334792.pdf";
        public string inalidPackingSlipUrl = "https://hsi.hemasource.com/File/GetCert?type=SOC1323N%20Baxter%20Saline&name=V20D05C.pdf";
        [Test]
        public void PackingSlip_Assert200ResponseDisplays()
        {
            driver.Navigate().GoToUrl(packingSlipUrl);
        }

        [Test]
        public void TestGetAllEndPoint()
        {
            // Step 1. To create the HTTP client
            HttpClient httpClient = new HttpClient();
            // Step 2 & 3. Create the request and execute it
            httpClient.GetAsync(packingSlipUrl);
            // Close the connection
            httpClient.Dispose();
        }
        
        [Test]
        public void TestGetAllEndPointWithUri()
        {
            // Step 1. To create the HTTP client
            HttpClient httpClient = new HttpClient();
            // Step 2 & 3. Create the request and execute it
            Task<HttpResponseMessage> httpResponse = httpClient.GetAsync(packingSlipUrl);
            HttpResponseMessage httpResponseMessage = httpResponse.Result;
            Console.WriteLine(httpResponseMessage.ToString());
            HttpStatusCode statusCode = httpResponseMessage.StatusCode;
            int StatusCodeInt = (int)statusCode;
            Assert.IsTrue(StatusCodeInt == 200);

            // Close the connection
            httpClient.Dispose();
        }

        [Test]
        public void TestGetAllEndPointWithInvalidUri()
        {
            // Step 1. To create the HTTP client
            HttpClient httpClient = new HttpClient();
            // Step 2 & 3. Create the request and execute it
            Task<HttpResponseMessage> httpResponse = httpClient.GetAsync(inalidPackingSlipUrl);
            HttpResponseMessage httpResponseMessage = httpResponse.Result;
            Console.WriteLine(httpResponseMessage.ToString());
            HttpStatusCode statusCode = httpResponseMessage.StatusCode;
            int StatusCodeInt = (int)statusCode;
            Assert.IsTrue(StatusCodeInt == 200);

            // Close the connection
            httpClient.Dispose();
        }

        [TearDown]
        public void TearDownTest()
        {
            driver.Close();
        }

        [SetUp]
        public void SetupTest()
        {
            // homeURL = "https://qa19r2.hemasource.com/";
            driver = new ChromeDriver();

        }
    }
}

