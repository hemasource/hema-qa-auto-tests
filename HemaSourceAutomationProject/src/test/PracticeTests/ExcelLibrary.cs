﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using ExcelDataReader;
using ExcelDataReader;

namespace HemaSourceAutomationProject.src.test.PracticeTests
{
    public class ExcelLibrary
    {
        public static List<Datacollection> dataCol;

        private static DataTable ExcelToDataTable(string fileName)
        {
            // open file and return as Stream
            string localPath = new Uri(fileName).LocalPath;
            FileStream stream = File.Open(localPath, FileMode.Open, FileAccess.Read);
            // Create open xml reader via ExcelReaderFactory
            IExcelDataReader excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream); // .xlsx

            // Set the First Row as Column Name
            // Return as Dataset
            DataSet result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
            {
                ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
            });

            // Get all the Tables
            DataTableCollection table = result.Tables;
            // Store it in DataTable
            DataTable resultTable = table["Sheet1"];
            // Return
            return resultTable;
            }

            public static void PopulateInCollection()
            {
                dataCol = new List<Datacollection>();
                string filepath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
                DataTable table = ExcelToDataTable(filepath + "\\DataPools\\HA_HotelSearch.xlsx");
            }
    }

        public class Datacollection
        {
            public int rowNumber { get; set; }
            public string colName { get; set; }
            public string ColValue { get; set; }
        }
}





